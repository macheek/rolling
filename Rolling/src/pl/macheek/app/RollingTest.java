package pl.macheek.app;

import pl.macheek.app.gui.*;

import java.awt.EventQueue;
import org.apache.logging.log4j.*;

public class RollingTest {
	private static Logger logger = LogManager.getLogger("pl.macheek.app.RollingTest");

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RollingFrame frame = new RollingFrame();
					frame.setVisible(true);
					logger.info("Tworzenie głównego okna programu klasy: " + frame.getClass());
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("Wystąpił wyjątek: " + e.getMessage());
				}
			}
		});
	}
}
