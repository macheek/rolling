package pl.macheek.app.business;

import pl.macheek.app.dto.*;
import pl.macheek.app.err.*;

import java.text.DecimalFormat;
import javax.swing.JOptionPane;

import org.apache.logging.log4j.*;

public class RollingCalc {
	private double heightIn;
	private double heightOut;
	private double widthIn;
	private double reIn;
	private double dRoll;
	private double dNeck;
	private double nMotor;
	private double reduction;
	private double subSigma;
	private double efficiency;

	private double rRoll;
	private double reKg;
	private double hToD;
	private double efficiencyD;
	private double pressure;
	private double nRoll;

	private double absSqueeze;
	private double relSqueeze;
	private double extension;
	private double width;
	private double force;
	private double forceNm;
	private double rForce;
	private double momentRoll;
	private double momentRollNm;
	private double torqueNm;
	private double torque;
	private double powerMotor;
	private double grip;

	private static Logger logger = LogManager.getLogger("pl.macheek.app.business.RollingCalc");

	/*
	 * Metoda do obliczania wymogów stawianych walcarce DUO
	 * 
	 * @param rollingInDTO
	 *            - pobieranie danych
	 * @param rollingOutDTO
	 *            - wyświetlanie danych
	 * @throws Exception
	 *             - kiedy pola "textField" są puste
	 */
	public void calculate(RollingInDTO rollingInDTO, RollingOutDTO rollingOutDTO) throws Exception {
		try {
			/*
			 * pobieranie danych
			 */
			logger.info("Pobieranie wprowadzonych danych.");
			heightIn = Double.parseDouble(rollingInDTO.getPlateIn().trim().replace(",", "."));
			heightOut = Double.parseDouble(rollingInDTO.getPlateOut().trim().replace(",", "."));
			widthIn = Double.parseDouble(rollingInDTO.getWidthIn().trim().replace(",", "."));
			reIn = Double.parseDouble(rollingInDTO.getRe().trim().replace(",", "."));
			dRoll = Double.parseDouble(rollingInDTO.getDRoll().trim().replace(",", "."));
			dNeck = Double.parseDouble(rollingInDTO.getDNeck().trim().replace(",", "."));
			nMotor = Double.parseDouble(rollingInDTO.getNMotor().trim().replace(",", "."));
			reduction = Double.parseDouble(rollingInDTO.getReduction().trim().replace(",", "."));
			/*
			 * Sprawdzenie czy podane wartości grubości materiału są poprawne
			 */
			logger.info("Sprawdzenie wartości grubości materiału.");
			if (heightIn < heightOut) {
				throw new RollingException(
						"Grubość materiału wyjściowego nie może być większa niż grubość materiału wejściowego!",
						"Uwaga!", JOptionPane.WARNING_MESSAGE, "materiał wyjściowy grubszy od materiału wejściowego");
			}
			/*
			 * Zastępcze naprężenia Aluminium lub miedź
			 */
			logger.info("Dobór naprężenia zastępczego i sprawności procesu.");
			if (rollingInDTO.getMaterial() == 1 || rollingInDTO.getMaterial() == 2) {
				if (relSqueeze <= 0.1) {
					subSigma = 18;
				} else if (relSqueeze > 0.1 && relSqueeze <= 0.2) {
					subSigma = 25;
				} else if (relSqueeze > 0.2 && relSqueeze <= 0.3) {
					subSigma = 30;
				} else if (relSqueeze > 0.3 && relSqueeze <= 0.4) {
					subSigma = 33;
				} else if (relSqueeze > 0.4 && relSqueeze <= 0.5) {
					subSigma = 34;
				} else {
					subSigma = 35;
				}
			}
			/*
			 * Mosiądz
			 */
			else if (rollingInDTO.getMaterial() == 3) {
				if (reKg <= 9) {
					if (relSqueeze <= 0.1) {
						subSigma = 25;
					} else if (relSqueeze > 0.1 && relSqueeze <= 0.2) {
						subSigma = 38;
					} else if (relSqueeze > 0.2 && relSqueeze <= 0.3) {
						subSigma = 46;
					} else if (relSqueeze > 0.3 && relSqueeze <= 0.4) {
						subSigma = 51;
					} else if (relSqueeze > 0.4 && relSqueeze <= 0.5) {
						subSigma = 53;
					} else {
						subSigma = 54;
					}
				} else {
					if (relSqueeze <= 0.1) {
						subSigma = 25;
					} else if (relSqueeze > 0.1 && relSqueeze <= 0.2) {
						subSigma = 37;
					} else if (relSqueeze > 0.2 && relSqueeze <= 0.3) {
						subSigma = 46;
					} else if (relSqueeze > 0.3 && relSqueeze <= 0.4) {
						subSigma = 52;
					} else if (relSqueeze > 0.4 && relSqueeze <= 0.5) {
						subSigma = 55;
					} else {
						subSigma = 57;
					}
				}
			}
			/*
			 * Brąz
			 */
			else if (rollingInDTO.getMaterial() == 4) {
				if (reKg <= 15) {
					if (relSqueeze <= 0.1) {
						subSigma = 30;
					} else if (relSqueeze > 0.1 && relSqueeze <= 0.2) {
						subSigma = 39;
					} else if (relSqueeze > 0.2 && relSqueeze <= 0.3) {
						subSigma = 43;
					} else if (relSqueeze > 0.3 && relSqueeze <= 0.4) {
						subSigma = 44;
					} else if (relSqueeze > 0.4 && relSqueeze <= 0.5) {
						subSigma = 45;
					} else {
						subSigma = 45;
					}
				} else {
					if (relSqueeze <= 0.1) {
						subSigma = 30;
					} else if (relSqueeze > 0.1 && relSqueeze <= 0.2) {
						subSigma = 41;
					} else if (relSqueeze > 0.2 && relSqueeze <= 0.3) {
						subSigma = 51;
					} else if (relSqueeze > 0.3 && relSqueeze <= 0.4) {
						subSigma = 58;
					} else if (relSqueeze > 0.4 && relSqueeze <= 0.5) {
						subSigma = 62;
					} else {
						subSigma = 65;
					}
				}
			}
			/*
			 * Stal miękka
			 */
			else if (rollingInDTO.getMaterial() == 5) {
				if (reKg <= 18) {
					if (relSqueeze <= 0.1) {
						subSigma = 45;
					} else if (relSqueeze > 0.1 && relSqueeze <= 0.2) {
						subSigma = 54;
					} else if (relSqueeze > 0.2 && relSqueeze <= 0.3) {
						subSigma = 59;
					} else if (relSqueeze > 0.3 && relSqueeze <= 0.4) {
						subSigma = 62;
					} else if (relSqueeze > 0.4 && relSqueeze <= 0.5) {
						subSigma = 63;
					} else {
						subSigma = 64;
					}
				} else {
					if (relSqueeze <= 0.1) {
						subSigma = 50;
					} else if (relSqueeze > 0.1 && relSqueeze <= 0.2) {
						subSigma = 60;
					} else if (relSqueeze > 0.2 && relSqueeze <= 0.3) {
						subSigma = 66;
					} else if (relSqueeze > 0.3 && relSqueeze <= 0.4) {
						subSigma = 70;
					} else if (relSqueeze > 0.4 && relSqueeze <= 0.5) {
						subSigma = 72;
					} else {
						subSigma = 73;
					}
				}
			}
			/*
			 * Stal konstrukcyjna
			 */
			else if (rollingInDTO.getMaterial() == 6) {
				if (relSqueeze <= 0.1) {
					subSigma = 68;
				} else if (relSqueeze > 0.1 && relSqueeze <= 0.2) {
					subSigma = 80;
				} else if (relSqueeze > 0.2 && relSqueeze <= 0.3) {
					subSigma = 85;
				} else if (relSqueeze > 0.3 && relSqueeze <= 0.4) {
					subSigma = 89;
				} else if (relSqueeze > 0.4 && relSqueeze <= 0.5) {
					subSigma = 91;
				} else {
					subSigma = 93;
				}
			}
			/*
			 * Stal twarda
			 */
			else if (rollingInDTO.getMaterial() == 7) {
				if (relSqueeze <= 0.1) {
					subSigma = 108;
				} else if (relSqueeze > 0.1 && relSqueeze <= 0.2) {
					subSigma = 117;
				} else if (relSqueeze > 0.2 && relSqueeze <= 0.3) {
					subSigma = 121;
				} else if (relSqueeze > 0.3 && relSqueeze <= 0.4) {
					subSigma = 123;
				} else if (relSqueeze > 0.4 && relSqueeze <= 0.5) {
					subSigma = 125;
				} else {
					subSigma = 126;
				}
			} else {
				subSigma = 0;
			}
			/*
			 * Sprawność procesu
			 */
			if (rollingInDTO.getMaterial() == 1 || rollingInDTO.getMaterial() == 2 || rollingInDTO.getMaterial() == 3
					|| rollingInDTO.getMaterial() == 4) {
				if (hToD <= 0.3) {
					efficiency = 55;
				} else if (hToD > 0.3 && hToD <= 0.5) {
					efficiency = 63;
				} else {
					efficiency = 75;
				}
			} else if (rollingInDTO.getMaterial() == 5) {
				if (rollingInDTO.getLubrication().equals("noLube")) {
					if (hToD <= 0.3) {
						efficiency = 45;
					} else if (hToD > 0.3 && hToD <= 0.5) {
						efficiency = 70;
					} else if (hToD > 0.5 && hToD <= 1) {
						efficiency = 68;
					} else if (hToD > 1 && hToD <= 1.5) {
						efficiency = 78;
					} else {
						efficiency = 80;
					}
				} else {
					if (hToD <= 0.3) {
						efficiency = 55;
					} else if (hToD > 0.3 && hToD <= 0.5) {
						efficiency = 75;
					} else {
						efficiency = 83;
					}
				}
			} else if (rollingInDTO.getMaterial() == 6) {
				if (hToD <= 0.5) {
					efficiency = 55;
				} else if (hToD > 0.5 && hToD <= 1) {
					efficiency = 76;
				} else if (hToD > 1 && hToD <= 1.5) {
					efficiency = 68;
				} else {
					efficiency = 80;
				}
			} else if (rollingInDTO.getMaterial() == 7) {
				if (hToD <= 0.5) {
					efficiency = 45;
				} else {
					efficiency = 70;
				}
			} else {
				throw new RollingException("Wybierz materiał!", "Uwaga!", JOptionPane.WARNING_MESSAGE,
						"brak wyboru materiału");
			}
			/*
			 * obliczenia pośrednie
			 */
			logger.info("Wykonywanie obliczeń.");
			rRoll = dRoll / 2;
			reKg = reIn * 0.10197;
			hToD = (heightIn / dRoll) * 100;
			efficiencyD = efficiency * 0.01;
			pressure = subSigma / efficiencyD;
			nRoll = nMotor / reduction;
			/*
			 * obliczenia końcowe
			 */
			absSqueeze = heightIn - heightOut;
			relSqueeze = absSqueeze / heightIn;
			extension = (absSqueeze / 6) * Math.sqrt((rRoll / heightIn));
			width = widthIn + extension;
			force = (pressure * ((widthIn + width) / 2) * (Math.sqrt((rRoll * absSqueeze))));
			forceNm = (force * 9.80665) / 1000;
			rForce = 0.5 * Math.sqrt((absSqueeze * rRoll));
			momentRoll = (force * 0.003 * dNeck) + (2 * force * rForce);
			momentRollNm = (momentRoll * 9.80665) * 0.001;
			torqueNm = (((nRoll / nMotor) / 0.95) * momentRoll) * 0.001;
			torque = torqueNm * 9.80665;
			powerMotor = (torqueNm * nMotor) / 973.8;
			grip = (Math.acos(1 - (absSqueeze / dRoll))) * (180 / Math.PI);
			/*
			 * Sprawdzenie warunku kąta chwytu
			 */
			logger.info("Sprawdzanie warunku kąta chwytu.");
			try {
				String sOK = "Warunek Spełniony";
				String sNOT = "Maksymalny kąt chwytu to";
				if (rollingInDTO.getTemperature().equals("hot")) {
					if (grip < 20) {
						rollingOutDTO.setGripCondiColor("GREEN");
						rollingOutDTO.setGripCondi(sOK);
					} else {
						rollingOutDTO.setGripCondiColor("RED");
						rollingOutDTO.setGripCondi(sNOT + " 20!");
					}
				} else {
					if (grip < 10) {
						rollingOutDTO.setGripCondiColor("GREEN");
						rollingOutDTO.setGripCondi(sOK);
					} else {
						rollingOutDTO.setGripCondiColor("RED");
						rollingOutDTO.setGripCondi(sNOT + " 10!");
					}
				}
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			logger.info("Wykonywanie obliczeń zakończono pomyślnie.");
			/*
			 * generowanie łańcuchów tekstowych
			 */
			DecimalFormat df = new DecimalFormat("#0.00");
			String sAbsSqueeze = df.format(absSqueeze);
			String sRelSqueeze = df.format(relSqueeze);
			String sExtension = df.format(extension);
			String sGrip = df.format(grip);
			String sForceNm = df.format(forceNm);
			String sMomentRollNm = df.format(momentRollNm);
			String sTorque = df.format(torque);
			String sPowerMotor = df.format(powerMotor);
			/*
			 * eksport danych
			 */
			logger.info("Eksport obliczonych danych w celu wyświetlenia w panelu resultsPanel.");
			rollingOutDTO.setAbsSqueeze(sAbsSqueeze);
			rollingOutDTO.setRelSqueeze(sRelSqueeze);
			rollingOutDTO.setExtension(sExtension);
			rollingOutDTO.setGrip(sGrip);
			rollingOutDTO.setForce(sForceNm);
			rollingOutDTO.setMomentRoll(sMomentRollNm);
			rollingOutDTO.setTorque(sTorque);
			rollingOutDTO.setPowerMotor(sPowerMotor);
			rollingOutDTO.setIPstate(false);
			rollingOutDTO.setRPstate(true);
		} catch (NumberFormatException e) {
			throw new RollingException("Wypełnij wszystkie pola!", "Uwaga!", JOptionPane.WARNING_MESSAGE,
					e.getMessage());
		}
	};
}
