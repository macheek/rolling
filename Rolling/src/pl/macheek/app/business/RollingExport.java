package pl.macheek.app.business;

import java.io.*;

import org.apache.logging.log4j.*;

import pl.macheek.app.dto.RollingExportDTO;

public class RollingExport {
	private FileWriter fw;

	private static Logger logger = LogManager.getLogger("pl.macheek.app.business.RollingExport");

	/*
	 * @param exportDTO
	 *            - pobieranie danych
	 */
	public void rollingExport(RollingExportDTO exportDTO) {

		try {
			logger.info("Eksport danych do pliku");
			fw = new FileWriter(new File("wyniki.txt"));
			fw.write("Gniot bezwględny: " + exportDTO.getAbsSqueeze() + "[mm]");
			fw.write(System.lineSeparator());
			fw.write("Gniot względny: " + exportDTO.getRelSqueeze() + "[mm]");
			fw.write(System.lineSeparator());
			fw.write("Poszerzenie: " + exportDTO.getExtension() + "[mm]");
			fw.write(System.lineSeparator());
			fw.write("Kąt chwytu: " + exportDTO.getGrip() + "[st]");
			fw.write(System.lineSeparator());
			fw.write("Nacisk walców: " + exportDTO.getForce() + "[kN]");
			fw.write(System.lineSeparator());
			fw.write("Moment walcowania: " + exportDTO.getMomentRoll() + "[Nm]");
			fw.write(System.lineSeparator());
			fw.write("Moment obrotowy: " + exportDTO.getTorque() + "[Nm]");
			fw.write(System.lineSeparator());
			fw.write("Moc silnika: " + exportDTO.getPowerMotor() + "[kW]");
			fw.write(System.lineSeparator());
			fw.close();
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
}
