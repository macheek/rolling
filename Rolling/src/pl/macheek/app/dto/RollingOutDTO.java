package pl.macheek.app.dto;

import java.awt.Color;

public class RollingOutDTO {
	private String absSqueeze;
	private String relSqueeze;
	private String extension;
	private String grip;
	private String force;
	private String momentRoll;
	private String torque;
	private String powerMotor;
	private String gripCondi;
	private String colorOut;
	private boolean iPstate;
	private boolean rPstate;

	public void setAbsSqueeze(String absSqueeze) {
		this.absSqueeze = absSqueeze;
	}

	public String getAbsSqueeze() {
		return absSqueeze;
	}

	public void setRelSqueeze(String relSqueeze) {
		this.relSqueeze = relSqueeze;
	}

	public String getRelSqueeze() {
		return relSqueeze;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getExtension() {
		return extension;
	}

	public void setGrip(String grip) {
		this.grip = grip;
	}

	public String getGrip() {
		return grip;
	}

	public void setForce(String force) {
		this.force = force;
	}

	public String getForce() {
		return force;
	}

	public void setMomentRoll(String momentRoll) {
		this.momentRoll = momentRoll;
	}

	public String getMomentRoll() {
		return momentRoll;
	}

	public void setTorque(String torque) {
		this.torque = torque;
	}

	public String getTorque() {
		return torque;
	}

	public void setPowerMotor(String powerMotor) {
		this.powerMotor = powerMotor;
	}

	public String getPowerMotor() {
		return powerMotor;
	}

	public void setGripCondi(String gripCondi) {
		this.gripCondi = gripCondi;
	}

	public String getGripCondi() {
		return gripCondi;
	}

	public void setGripCondiColor(String colorOut) {
		this.colorOut = colorOut;
	}

	public Color getGripCondiColor() {
		if (colorOut.equals("RED"))
			return Color.RED;
		else
			return Color.GREEN;
	}

	public void setIPstate(boolean iPstate) {
		this.iPstate = iPstate;
	}

	public boolean getIPstate() {
		return iPstate;
	}

	public void setRPstate(boolean rPstate) {
		this.rPstate = rPstate;
	}

	public boolean getRPstate() {
		return rPstate;
	}
}
