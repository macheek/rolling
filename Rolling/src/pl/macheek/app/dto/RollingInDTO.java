package pl.macheek.app.dto;

public class RollingInDTO 
{
	private String plateIn;
	private String plateOut;
	private String widthIn;
	private String re;
	private String dRoll;
	private String dNeck;
	private String nMotor;
	private String reduction;	
	private int material;
	private String lube;
	private String temperature;
	
	public void setPlateIn(String plateIn)
	{
		this.plateIn = plateIn;
	}
	public String getPlateIn()
	{
		return plateIn;
	}
	public void setPlateOut(String plateOut)
	{
		this.plateOut = plateOut;
	}
	public String getPlateOut()
	{
		return plateOut;
	}
	public void setWidthIn(String widthIn)
	{
		this.widthIn = widthIn;
	}
	public String getWidthIn()
	{
		return widthIn;
	}
	public void setRe(String re)
	{
		this.re = re;
	}
	public String getRe()
	{
		return re;
	}
	public void setDRoll(String dRoll)
	{
		this.dRoll = dRoll;
	}
	public String getDRoll()
	{
		return dRoll;
	}
	public void setDNeck(String dNeck)
	{
		this.dNeck = dNeck;
	}
	public String getDNeck()
	{
		return dNeck;
	}
	public void setNMotor(String nMotor)
	{
		this.nMotor = nMotor;
	}
	public String getNMotor()
	{
		return nMotor;
	}
	public void setReduction(String reduction)
	{
		this.reduction = reduction;
	}
	public String getReduction()
	{
		return reduction;
	}
	public void setMaterial (int material)
	{
		this.material = material;
	}
	public int getMaterial()
	{
		return material;
	}
	public void setLubrication(String lube)
	{
		this.lube = lube;
	}
	public String getLubrication()
	{
		return lube;
	}
	public void setTemperature(String temperature)
	{
		this.temperature = temperature;
	}
	public String getTemperature()
	{
		return temperature;
	}
}