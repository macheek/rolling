package pl.macheek.app.err;

public class RollingException extends Exception {

	private String title;
	private int icon;
	private String reason;

	/*
	 * @param msg
	 *            - wyświetlany komunikat
	 * @param title
	 *            - tytuł okna z wyświetlanym komunikatem
	 * @param icon
	 *            - ikona wyświetlanego komunikatu
	 * @param reason
	 *            - powód wyświetlenia okna komunikatu (używane jedynie w
	 *            loggerze)
	 */
	public RollingException(String msg, String title, int icon, String reason) {
		super(msg);
		this.title = title;
		this.setIcon(icon);
		this.reason = reason;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public int getIcon() {
		return icon;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReason() {
		return reason;
	}
}
