package pl.macheek.app.gui;

import pl.macheek.app.business.*;

import pl.macheek.app.dto.*;
import pl.macheek.app.err.RollingException;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.*;

import java.awt.CardLayout;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.GridBagLayout;
import javax.swing.JTextField;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.ButtonGroup;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/*
 * @author Macheek
 *
 */
public class RollingFrame extends JFrame {
	private FileWriter fw;

	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenuItem mntmNew;
	private JMenuItem mntmQuit;
	private JPanel contentPane;
	private JPanel startPanel;
	public JPanel inputPanel;
	public JPanel resultsPanel;

	private JTextPane txtFieldGreeting;

	private JLabel lblPlateIn;
	private JLabel lblPlateOut;
	private JButton btnCalc;
	private JButton btnReset;

	private JLabel lblMat;
	private JLabel lblRe;
	private JLabel lblLube;
	private JLabel lblTemp;
	private JLabel lblDRoll;
	private JLabel lblDNeck;
	private JLabel lblNMotor;
	private JLabel lblReduction;
	private JLabel lblWidthIn;

	public JTextField textFieldPlateIn;
	public JTextField textFieldPlateOut;
	public JTextField textFieldDRoll;
	public JTextField textFieldDNeck;
	public JTextField textFieldNMotor;
	public JTextField textFieldReduction;
	public JTextField textFieldWidthIn;
	public JTextField textFieldRe;
	public JComboBox cbMat;

	public ButtonGroup lubrication;
	public JRadioButton rdbtnNoLube;
	public JRadioButton rdbtnWithLube;
	public String noLube;
	public String withLube;

	public ButtonGroup temperature;
	public JRadioButton rdbtnCold;
	public JRadioButton rdbtnHot;
	public String cold;
	public String hot;

	private JLabel lblAbsSqueeze;
	private JLabel lblRelSqueeze;
	private JLabel lblExtension;
	private JLabel lblGrip;
	private JLabel lblForce;
	private JLabel lblMomentRoll;
	private JLabel lblTorque;
	private JLabel lblPowerMotor;

	public JTextField textFieldAbsSqueeze;
	public JTextField textFieldRelSqueeze;
	public JTextField textFieldExtension;
	public JTextField textFieldGrip;
	public JTextField textFieldGripCondi;
	public JTextField textFieldForce;
	public JTextField textFieldMomentRoll;
	public JTextField textFieldTorque;
	public JTextField textFieldPowerMotor;

	private static Logger logger = LogManager.getLogger("pl.macheek.app.gui.RollingFrame");
	private JButton btnExport;
	private JButton btnChange;
	private JButton btnNew;
	private JButton btnQuit;

	/*
	 * Tworzenie głównej ramki
	 */
	public RollingFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 350);
		setTitle("Walcarka");
		setLocationRelativeTo(null);
		/*
		 * Menu górne
		 */
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mnFile = new JMenu("Plik");
		menuBar.add(mnFile);

		mntmNew = new JMenuItem("Nowy");
		mntmNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputPanel.setVisible(true);
				startPanel.setVisible(false);
				resultsPanel.setVisible(false);
				logger.info("Otworzenie nowego pliku.");
			}
		});
		mnFile.add(mntmNew);

		mntmQuit = new JMenuItem("Zakończ");
		mntmQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.info("Wyłączenie programu");
				System.exit(0);
			}
		});
		mnFile.add(mntmQuit);
		/*
		 * CardLayout
		 */
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		/*
		 * Dodawanie paneli
		 */
		startPanel = new JPanel();
		contentPane.add(startPanel, "name_21428773561261");
		startPanel.setVisible(true);

		inputPanel = new JPanel();
		contentPane.add(inputPanel, "name_21440123179327");
		inputPanel.setVisible(false);

		resultsPanel = new JPanel();
		contentPane.add(resultsPanel, "name_21441615614399");
		resultsPanel.setVisible(false);
		/*
		 * Layout
		 */
		startPanel.setLayout(new BorderLayout(0, 0));

		GridBagLayout gblInputPanel = new GridBagLayout();
		gblInputPanel.columnWidths = new int[] { 150, 150, 150, 150 };
		gblInputPanel.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 30, 30, 30 };
		gblInputPanel.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0 };
		gblInputPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
		inputPanel.setLayout(gblInputPanel);

		GridBagLayout gblResultsPanel = new GridBagLayout();
		gblResultsPanel.columnWidths = new int[] { 150, 150, 150, 150 };
		gblResultsPanel.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 30, 30 };
		gblResultsPanel.columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0 };
		gblResultsPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
		resultsPanel.setLayout(gblResultsPanel);
		/*
		 * Elementy panelu startPanel
		 */
		txtFieldGreeting = new JTextPane();
		txtFieldGreeting.setEditable(false);
		txtFieldGreeting.setBackground(UIManager.getColor("Panel.background"));
		txtFieldGreeting.setFont(new Font("Times New Roman", Font.PLAIN, 24));
		txtFieldGreeting.setText("Program służący do obliczania wymogów stawianych walcarce DUO");
		startPanel.add(txtFieldGreeting, BorderLayout.CENTER);
		/*
		 * Elementy panelu inputPanel
		 */
		lblPlateIn = new JLabel("Grubość blachy wej. [mm]:");
		lblPlateIn.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbcLblPlateIn = new GridBagConstraints();
		gbcLblPlateIn.anchor = GridBagConstraints.EAST;
		gbcLblPlateIn.insets = new Insets(0, 0, 5, 5);
		gbcLblPlateIn.gridx = 0;
		gbcLblPlateIn.gridy = 0;
		inputPanel.add(lblPlateIn, gbcLblPlateIn);

		textFieldPlateIn = new JTextField();
		textFieldPlateIn.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c = evt.getKeyChar();
				if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE
						|| c == KeyEvent.VK_PERIOD || c == KeyEvent.VK_COMMA))
					evt.consume();
			}
		});
		GridBagConstraints gbcTextFieldPlateIn = new GridBagConstraints();
		gbcTextFieldPlateIn.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldPlateIn.insets = new Insets(0, 0, 5, 5);
		gbcTextFieldPlateIn.gridx = 1;
		gbcTextFieldPlateIn.gridy = 0;
		inputPanel.add(textFieldPlateIn, gbcTextFieldPlateIn);
		textFieldPlateIn.setColumns(10);

		lblPlateOut = new JLabel("Grubość blachy wyj. [mm]:");
		lblPlateOut.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbcLblPlateOut = new GridBagConstraints();
		gbcLblPlateOut.anchor = GridBagConstraints.EAST;
		gbcLblPlateOut.insets = new Insets(0, 0, 5, 5);
		gbcLblPlateOut.gridx = 2;
		gbcLblPlateOut.gridy = 0;
		inputPanel.add(lblPlateOut, gbcLblPlateOut);

		textFieldPlateOut = new JTextField();
		textFieldPlateOut.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c = evt.getKeyChar();
				if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE
						|| c == KeyEvent.VK_PERIOD || c == KeyEvent.VK_COMMA))
					evt.consume();
			}
		});
		GridBagConstraints gbcTextFieldPlateOut = new GridBagConstraints();
		gbcTextFieldPlateOut.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldPlateOut.insets = new Insets(0, 0, 5, 0);
		gbcTextFieldPlateOut.gridx = 3;
		gbcTextFieldPlateOut.gridy = 0;
		inputPanel.add(textFieldPlateOut, gbcTextFieldPlateOut);
		textFieldPlateOut.setColumns(10);

		lblWidthIn = new JLabel("Szerokość blachy [mm]:");
		GridBagConstraints gbcLblWidthIn = new GridBagConstraints();
		gbcLblWidthIn.anchor = GridBagConstraints.EAST;
		gbcLblWidthIn.insets = new Insets(0, 0, 5, 5);
		gbcLblWidthIn.gridx = 0;
		gbcLblWidthIn.gridy = 1;
		inputPanel.add(lblWidthIn, gbcLblWidthIn);

		textFieldWidthIn = new JTextField();
		textFieldWidthIn.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c = evt.getKeyChar();
				if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE
						|| c == KeyEvent.VK_PERIOD || c == KeyEvent.VK_COMMA))
					evt.consume();
			}
		});
		GridBagConstraints gbcTextFieldWidthIn = new GridBagConstraints();
		gbcTextFieldWidthIn.insets = new Insets(0, 0, 5, 5);
		gbcTextFieldWidthIn.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldWidthIn.gridx = 1;
		gbcTextFieldWidthIn.gridy = 1;
		inputPanel.add(textFieldWidthIn, gbcTextFieldWidthIn);
		textFieldWidthIn.setColumns(10);
		/*
		 * Grupa przycisków smarowanie
		 */
		lubrication = new ButtonGroup();

		lblLube = new JLabel("Smarowanie:");
		GridBagConstraints gbcLblLube = new GridBagConstraints();
		gbcLblLube.anchor = GridBagConstraints.EAST;
		gbcLblLube.insets = new Insets(0, 0, 5, 5);
		gbcLblLube.gridx = 0;
		gbcLblLube.gridy = 3;
		inputPanel.add(lblLube, gbcLblLube);

		rdbtnNoLube = new JRadioButton("brak");
		rdbtnNoLube.setActionCommand("noLube");
		rdbtnNoLube.setSelected(true);
		GridBagConstraints gbcRdbtnNoLube = new GridBagConstraints();
		gbcRdbtnNoLube.anchor = GridBagConstraints.WEST;
		gbcRdbtnNoLube.insets = new Insets(0, 0, 5, 5);
		gbcRdbtnNoLube.gridx = 1;
		gbcRdbtnNoLube.gridy = 3;
		inputPanel.add(rdbtnNoLube, gbcRdbtnNoLube);
		lubrication.add(rdbtnNoLube);

		rdbtnWithLube = new JRadioButton("ze smarowaniem");
		rdbtnWithLube.setActionCommand("withLube");
		GridBagConstraints gbcRdbtnWithLube = new GridBagConstraints();
		gbcRdbtnWithLube.anchor = GridBagConstraints.WEST;
		gbcRdbtnWithLube.insets = new Insets(0, 0, 5, 5);
		gbcRdbtnWithLube.gridx = 2;
		gbcRdbtnWithLube.gridy = 3;
		inputPanel.add(rdbtnWithLube, gbcRdbtnWithLube);
		lubrication.add(rdbtnWithLube);
		/*
		 * Grupa przycisków temperatura
		 */
		temperature = new ButtonGroup();

		lblTemp = new JLabel("Temperatura procesu:");
		GridBagConstraints gbcLblTemp = new GridBagConstraints();
		gbcLblTemp.anchor = GridBagConstraints.EAST;
		gbcLblTemp.insets = new Insets(0, 0, 5, 5);
		gbcLblTemp.gridx = 0;
		gbcLblTemp.gridy = 4;
		inputPanel.add(lblTemp, gbcLblTemp);

		rdbtnCold = new JRadioButton("na zimno");
		rdbtnCold.setActionCommand("cold");
		rdbtnCold.setSelected(true);
		GridBagConstraints gbcRdbtnCold = new GridBagConstraints();
		gbcRdbtnCold.anchor = GridBagConstraints.WEST;
		gbcRdbtnCold.insets = new Insets(0, 0, 5, 5);
		gbcRdbtnCold.gridx = 1;
		gbcRdbtnCold.gridy = 4;
		inputPanel.add(rdbtnCold, gbcRdbtnCold);
		temperature.add(rdbtnCold);

		rdbtnHot = new JRadioButton("na gorąco");
		rdbtnHot.setActionCommand("hot");
		GridBagConstraints gbcRdbtnHot = new GridBagConstraints();
		gbcRdbtnHot.anchor = GridBagConstraints.WEST;
		gbcRdbtnHot.insets = new Insets(0, 0, 5, 5);
		gbcRdbtnHot.gridx = 2;
		gbcRdbtnHot.gridy = 4;
		inputPanel.add(rdbtnHot, gbcRdbtnHot);
		temperature.add(rdbtnHot);

		lblMat = new JLabel("Wybór materiału:");
		lblMat.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbcLblMat = new GridBagConstraints();
		gbcLblMat.anchor = GridBagConstraints.EAST;
		gbcLblMat.insets = new Insets(0, 0, 5, 5);
		gbcLblMat.gridx = 0;
		gbcLblMat.gridy = 2;
		inputPanel.add(lblMat, gbcLblMat);

		String[] matString = { "(wybór materiału)", "Aluminium", "Miedź", "Mosiądz", "Brąz", "Stal miękka",
				"Stal konstrukcyjna", "Stal twarda" };
		cbMat = new JComboBox(matString);
		GridBagConstraints gbcCbMat = new GridBagConstraints();
		gbcCbMat.insets = new Insets(0, 0, 5, 5);
		gbcCbMat.fill = GridBagConstraints.HORIZONTAL;
		gbcCbMat.gridx = 1;
		gbcCbMat.gridy = 2;
		inputPanel.add(cbMat, gbcCbMat);

		lblRe = new JLabel("Granica plastyczności [MPa]:");
		GridBagConstraints gbcLblRe = new GridBagConstraints();
		gbcLblRe.anchor = GridBagConstraints.EAST;
		gbcLblRe.insets = new Insets(0, 0, 5, 5);
		gbcLblRe.gridx = 2;
		gbcLblRe.gridy = 2;
		inputPanel.add(lblRe, gbcLblRe);

		textFieldRe = new JTextField();
		textFieldRe.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c = evt.getKeyChar();
				if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE
						|| c == KeyEvent.VK_PERIOD || c == KeyEvent.VK_COMMA))
					evt.consume();
			}
		});
		GridBagConstraints gbcTextFieldRe = new GridBagConstraints();
		gbcTextFieldRe.insets = new Insets(0, 0, 5, 0);
		gbcTextFieldRe.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldRe.gridx = 3;
		gbcTextFieldRe.gridy = 2;
		inputPanel.add(textFieldRe, gbcTextFieldRe);
		textFieldRe.setColumns(10);

		lblDRoll = new JLabel("Średnica walców [mm]:");
		GridBagConstraints gbcLblDRoll = new GridBagConstraints();
		gbcLblDRoll.anchor = GridBagConstraints.EAST;
		gbcLblDRoll.insets = new Insets(0, 0, 5, 5);
		gbcLblDRoll.gridx = 0;
		gbcLblDRoll.gridy = 5;
		inputPanel.add(lblDRoll, gbcLblDRoll);

		textFieldDRoll = new JTextField();
		textFieldDRoll.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c = evt.getKeyChar();
				if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE
						|| c == KeyEvent.VK_PERIOD || c == KeyEvent.VK_COMMA))
					evt.consume();
			}
		});
		GridBagConstraints gbcTextFieldDRoll = new GridBagConstraints();
		gbcTextFieldDRoll.insets = new Insets(0, 0, 5, 5);
		gbcTextFieldDRoll.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldDRoll.gridx = 1;
		gbcTextFieldDRoll.gridy = 5;
		inputPanel.add(textFieldDRoll, gbcTextFieldDRoll);
		textFieldDRoll.setColumns(10);

		lblDNeck = new JLabel("Średnica czopów [mm]:");
		GridBagConstraints gbcLblDNeck = new GridBagConstraints();
		gbcLblDNeck.anchor = GridBagConstraints.EAST;
		gbcLblDNeck.insets = new Insets(0, 0, 5, 5);
		gbcLblDNeck.gridx = 2;
		gbcLblDNeck.gridy = 5;
		inputPanel.add(lblDNeck, gbcLblDNeck);

		textFieldDNeck = new JTextField();
		textFieldDNeck.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c = evt.getKeyChar();
				if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE
						|| c == KeyEvent.VK_PERIOD || c == KeyEvent.VK_COMMA))
					evt.consume();
			}
		});
		GridBagConstraints gbcTextFieldDNeck = new GridBagConstraints();
		gbcTextFieldDNeck.insets = new Insets(0, 0, 5, 0);
		gbcTextFieldDNeck.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldDNeck.gridx = 3;
		gbcTextFieldDNeck.gridy = 5;
		inputPanel.add(textFieldDNeck, gbcTextFieldDNeck);
		textFieldDNeck.setColumns(10);

		lblNMotor = new JLabel("P. obrotowa silnika [obr/min]:");
		GridBagConstraints gbcLblNMotor = new GridBagConstraints();
		gbcLblNMotor.anchor = GridBagConstraints.EAST;
		gbcLblNMotor.insets = new Insets(0, 0, 5, 5);
		gbcLblNMotor.gridx = 0;
		gbcLblNMotor.gridy = 6;
		inputPanel.add(lblNMotor, gbcLblNMotor);

		textFieldNMotor = new JTextField();
		textFieldNMotor.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c = evt.getKeyChar();
				if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE
						|| c == KeyEvent.VK_PERIOD || c == KeyEvent.VK_COMMA))
					evt.consume();
			}
		});
		GridBagConstraints gbcTextFieldNMotor = new GridBagConstraints();
		gbcTextFieldNMotor.insets = new Insets(0, 0, 5, 5);
		gbcTextFieldNMotor.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldNMotor.gridx = 1;
		gbcTextFieldNMotor.gridy = 6;
		inputPanel.add(textFieldNMotor, gbcTextFieldNMotor);
		textFieldNMotor.setColumns(10);

		lblReduction = new JLabel("Redukcja:");
		GridBagConstraints gbcLblReduction = new GridBagConstraints();
		gbcLblReduction.anchor = GridBagConstraints.EAST;
		gbcLblReduction.insets = new Insets(0, 0, 5, 5);
		gbcLblReduction.gridx = 2;
		gbcLblReduction.gridy = 6;
		inputPanel.add(lblReduction, gbcLblReduction);

		textFieldReduction = new JTextField();
		textFieldReduction.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent evt) {
				char c = evt.getKeyChar();
				if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE
						|| c == KeyEvent.VK_PERIOD || c == KeyEvent.VK_COMMA))
					evt.consume();
			}
		});
		GridBagConstraints gbcTextFieldReduction = new GridBagConstraints();
		gbcTextFieldReduction.insets = new Insets(0, 0, 5, 0);
		gbcTextFieldReduction.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldReduction.gridx = 3;
		gbcTextFieldReduction.gridy = 6;
		inputPanel.add(textFieldReduction, gbcTextFieldReduction);
		textFieldReduction.setColumns(10);

		btnCalc = new JButton("Oblicz!");
		GridBagConstraints gbcBtnCalc = new GridBagConstraints();
		gbcBtnCalc.anchor = GridBagConstraints.EAST;
		gbcBtnCalc.insets = new Insets(0, 0, 5, 5);
		gbcBtnCalc.gridx = 1;
		gbcBtnCalc.gridy = 7;
		inputPanel.add(btnCalc, gbcBtnCalc);
		btnCalc.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				RollingCalc calc = new RollingCalc();
				RollingInDTO idto = new RollingInDTO();
				RollingOutDTO odto = new RollingOutDTO();
				idto.setPlateIn(textFieldPlateIn.getText());
				idto.setPlateOut(textFieldPlateOut.getText());
				idto.setWidthIn(textFieldWidthIn.getText());
				idto.setRe(textFieldRe.getText());
				idto.setDRoll(textFieldDRoll.getText());
				idto.setDNeck(textFieldDNeck.getText());
				idto.setNMotor(textFieldNMotor.getText());
				idto.setReduction(textFieldReduction.getText());
				idto.setMaterial(cbMat.getSelectedIndex());
				idto.setLubrication(lubrication.getSelection().getActionCommand());
				idto.setTemperature(temperature.getSelection().getActionCommand());
				try {
					calc.calculate(idto, odto);
				} catch (RollingException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage(), e1.getTitle(), e1.getIcon());
					logger.error("Wystąpił błąd spowodowany: " + e1.getReason());
					return;
				} catch (Exception e1) {
					if (e1 != null && e1.getMessage() != null)
						JOptionPane.showMessageDialog(null, e1.getMessage());
					else
						JOptionPane.showMessageDialog(null, "An error occured");
				}
				logger.info("Zapełnianie pól wynikami.");
				textFieldAbsSqueeze.setText(odto.getAbsSqueeze());
				textFieldRelSqueeze.setText(odto.getRelSqueeze());
				textFieldExtension.setText(odto.getExtension());
				textFieldGrip.setText(odto.getGrip());
				textFieldForce.setText(odto.getForce());
				textFieldMomentRoll.setText(odto.getMomentRoll());
				textFieldTorque.setText(odto.getTorque());
				textFieldPowerMotor.setText(odto.getPowerMotor());
				textFieldGripCondi.setText(odto.getGripCondi());
				textFieldGripCondi.setBackground(odto.getGripCondiColor());
				logger.info("Przełączenie z panelu wprowadzania danych inputPanel do panelu z wynikami resultsPanel.");
				inputPanel.setVisible(odto.getIPstate());
				resultsPanel.setVisible(odto.getRPstate());
			}
		});

		btnReset = new JButton("Reset");
		GridBagConstraints gbcBtnReset = new GridBagConstraints();
		gbcBtnReset.insets = new Insets(0, 0, 5, 5);
		gbcBtnReset.anchor = GridBagConstraints.WEST;
		gbcBtnReset.gridx = 2;
		gbcBtnReset.gridy = 7;
		inputPanel.add(btnReset, gbcBtnReset);
		btnReset.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				logger.info("Resetowanie wprowadzonych danych.");
				textFieldPlateIn.setText(" ");
				textFieldPlateOut.setText(" ");
				textFieldWidthIn.setText(" ");
				textFieldRe.setText(" ");
				textFieldDRoll.setText(" ");
				textFieldDNeck.setText(" ");
				textFieldNMotor.setText(" ");
				textFieldReduction.setText(" ");
				rdbtnNoLube.setSelected(true);
				rdbtnWithLube.setSelected(false);
				rdbtnCold.setSelected(true);
				rdbtnHot.setSelected(false);
				cbMat.setSelectedIndex(0);
			}
		});
		/*
		 * Elementy panelu resultsPanel
		 */
		lblAbsSqueeze = new JLabel("Gniot bezwzględnydny [mm]:");
		GridBagConstraints gbcLblAbsSqueeze = new GridBagConstraints();
		gbcLblAbsSqueeze.anchor = GridBagConstraints.EAST;
		gbcLblAbsSqueeze.insets = new Insets(0, 0, 5, 5);
		gbcLblAbsSqueeze.gridx = 0;
		gbcLblAbsSqueeze.gridy = 0;
		resultsPanel.add(lblAbsSqueeze, gbcLblAbsSqueeze);

		textFieldAbsSqueeze = new JTextField();
		textFieldAbsSqueeze.setEditable(false);
		GridBagConstraints gbcTextFieldAbsSqueeze = new GridBagConstraints();
		gbcTextFieldAbsSqueeze.insets = new Insets(0, 0, 5, 5);
		gbcTextFieldAbsSqueeze.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldAbsSqueeze.gridx = 1;
		gbcTextFieldAbsSqueeze.gridy = 0;
		resultsPanel.add(textFieldAbsSqueeze, gbcTextFieldAbsSqueeze);
		textFieldAbsSqueeze.setColumns(10);

		lblRelSqueeze = new JLabel("Gniot względny:");
		GridBagConstraints gbcLblRelSqueeze = new GridBagConstraints();
		gbcLblRelSqueeze.anchor = GridBagConstraints.EAST;
		gbcLblRelSqueeze.insets = new Insets(0, 0, 5, 5);
		gbcLblRelSqueeze.gridx = 2;
		gbcLblRelSqueeze.gridy = 0;
		resultsPanel.add(lblRelSqueeze, gbcLblRelSqueeze);

		textFieldRelSqueeze = new JTextField();
		textFieldRelSqueeze.setEditable(false);
		GridBagConstraints gbcTextFieldRelSqueeze = new GridBagConstraints();
		gbcTextFieldRelSqueeze.insets = new Insets(0, 0, 5, 0);
		gbcTextFieldRelSqueeze.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldRelSqueeze.gridx = 3;
		gbcTextFieldRelSqueeze.gridy = 0;
		resultsPanel.add(textFieldRelSqueeze, gbcTextFieldRelSqueeze);
		textFieldRelSqueeze.setColumns(10);

		lblExtension = new JLabel("Poszerzenie [mm]:");
		GridBagConstraints gbcLblExtension = new GridBagConstraints();
		gbcLblExtension.anchor = GridBagConstraints.EAST;
		gbcLblExtension.insets = new Insets(0, 0, 5, 5);
		gbcLblExtension.gridx = 0;
		gbcLblExtension.gridy = 1;
		resultsPanel.add(lblExtension, gbcLblExtension);

		textFieldExtension = new JTextField();
		textFieldExtension.setEditable(false);
		GridBagConstraints gbcTextFieldExtension = new GridBagConstraints();
		gbcTextFieldExtension.insets = new Insets(0, 0, 5, 5);
		gbcTextFieldExtension.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldExtension.gridx = 1;
		gbcTextFieldExtension.gridy = 1;
		resultsPanel.add(textFieldExtension, gbcTextFieldExtension);
		textFieldExtension.setColumns(10);

		lblGrip = new JLabel("Kąt chwytu [st]:");
		GridBagConstraints gbcLblGrip = new GridBagConstraints();
		gbcLblGrip.anchor = GridBagConstraints.EAST;
		gbcLblGrip.insets = new Insets(0, 0, 5, 5);
		gbcLblGrip.gridx = 0;
		gbcLblGrip.gridy = 2;
		resultsPanel.add(lblGrip, gbcLblGrip);

		textFieldGrip = new JTextField();
		textFieldGrip.setEditable(false);
		GridBagConstraints gbcTextFieldGrip = new GridBagConstraints();
		gbcTextFieldGrip.insets = new Insets(0, 0, 5, 5);
		gbcTextFieldGrip.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldGrip.gridx = 1;
		gbcTextFieldGrip.gridy = 2;
		resultsPanel.add(textFieldGrip, gbcTextFieldGrip);
		textFieldGrip.setColumns(10);

		textFieldGripCondi = new JTextField();
		textFieldGripCondi.setEditable(false);
		GridBagConstraints gbcTextFieldGripCondi = new GridBagConstraints();
		gbcTextFieldGripCondi.insets = new Insets(0, 0, 5, 5);
		gbcTextFieldGripCondi.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldGripCondi.gridx = 2;
		gbcTextFieldGripCondi.gridy = 2;
		resultsPanel.add(textFieldGripCondi, gbcTextFieldGripCondi);
		textFieldGripCondi.setColumns(10);

		lblForce = new JLabel("Nacisk walców [kN]:");
		GridBagConstraints gbcLblForce = new GridBagConstraints();
		gbcLblForce.anchor = GridBagConstraints.EAST;
		gbcLblForce.insets = new Insets(0, 0, 5, 5);
		gbcLblForce.gridx = 0;
		gbcLblForce.gridy = 3;
		resultsPanel.add(lblForce, gbcLblForce);

		textFieldForce = new JTextField();
		textFieldForce.setEditable(false);
		GridBagConstraints gbcTextFieldForce = new GridBagConstraints();
		gbcTextFieldForce.insets = new Insets(0, 0, 5, 5);
		gbcTextFieldForce.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldForce.gridx = 1;
		gbcTextFieldForce.gridy = 3;
		resultsPanel.add(textFieldForce, gbcTextFieldForce);
		textFieldForce.setColumns(10);

		lblMomentRoll = new JLabel("Moment walcowania [Nm]:");
		GridBagConstraints gbcLblMomentRoll = new GridBagConstraints();
		gbcLblMomentRoll.anchor = GridBagConstraints.EAST;
		gbcLblMomentRoll.insets = new Insets(0, 0, 5, 5);
		gbcLblMomentRoll.gridx = 2;
		gbcLblMomentRoll.gridy = 3;
		resultsPanel.add(lblMomentRoll, gbcLblMomentRoll);

		textFieldMomentRoll = new JTextField();
		textFieldMomentRoll.setEditable(false);
		GridBagConstraints gbcTextFieldMomentRoll = new GridBagConstraints();
		gbcTextFieldMomentRoll.insets = new Insets(0, 0, 5, 0);
		gbcTextFieldMomentRoll.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldMomentRoll.gridx = 3;
		gbcTextFieldMomentRoll.gridy = 3;
		resultsPanel.add(textFieldMomentRoll, gbcTextFieldMomentRoll);
		textFieldMomentRoll.setColumns(10);

		lblTorque = new JLabel("Moment obrotowy [Nm]:");
		GridBagConstraints gbcLblTorque = new GridBagConstraints();
		gbcLblTorque.anchor = GridBagConstraints.EAST;
		gbcLblTorque.insets = new Insets(0, 0, 5, 5);
		gbcLblTorque.gridx = 0;
		gbcLblTorque.gridy = 4;
		resultsPanel.add(lblTorque, gbcLblTorque);

		textFieldTorque = new JTextField();
		textFieldTorque.setEditable(false);
		GridBagConstraints gbcTextFieldTorque = new GridBagConstraints();
		gbcTextFieldTorque.insets = new Insets(0, 0, 5, 5);
		gbcTextFieldTorque.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldTorque.gridx = 1;
		gbcTextFieldTorque.gridy = 4;
		resultsPanel.add(textFieldTorque, gbcTextFieldTorque);
		textFieldTorque.setColumns(10);

		lblPowerMotor = new JLabel("Moc silnika [kW]:");
		GridBagConstraints gbcLblPowerMotor = new GridBagConstraints();
		gbcLblPowerMotor.anchor = GridBagConstraints.EAST;
		gbcLblPowerMotor.insets = new Insets(0, 0, 5, 5);
		gbcLblPowerMotor.gridx = 2;
		gbcLblPowerMotor.gridy = 4;
		resultsPanel.add(lblPowerMotor, gbcLblPowerMotor);

		textFieldPowerMotor = new JTextField();
		textFieldPowerMotor.setEditable(false);
		GridBagConstraints gbcTextFieldPowerMotor = new GridBagConstraints();
		gbcTextFieldPowerMotor.insets = new Insets(0, 0, 5, 0);
		gbcTextFieldPowerMotor.fill = GridBagConstraints.HORIZONTAL;
		gbcTextFieldPowerMotor.gridx = 3;
		gbcTextFieldPowerMotor.gridy = 4;
		resultsPanel.add(textFieldPowerMotor, gbcTextFieldPowerMotor);
		textFieldPowerMotor.setColumns(10);
		/*
		 * Przycisk służący do eksportowania danych do pliku
		 */
		btnExport = new JButton("Eksport");
		GridBagConstraints gbcBtnExport = new GridBagConstraints();
		gbcBtnExport.anchor = GridBagConstraints.EAST;
		gbcBtnExport.insets = new Insets(0, 0, 5, 5);
		gbcBtnExport.gridx = 1;
		gbcBtnExport.gridy = 5;
		resultsPanel.add(btnExport, gbcBtnExport);
		btnExport.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				logger.info("Polecenie eksportu oblicznych danych do pliku wyniki.txt.");
				RollingExportDTO exportDTO = new RollingExportDTO();
				RollingExport export = new RollingExport();
				exportDTO.setAbsSqueeze(textFieldAbsSqueeze.getText());
				exportDTO.setRelSqueeze(textFieldRelSqueeze.getText());
				exportDTO.setExtension(textFieldExtension.getText());
				exportDTO.setGrip(textFieldGrip.getText());
				exportDTO.setForce(textFieldForce.getText());
				exportDTO.setMomentRoll(textFieldMomentRoll.getText());
				exportDTO.setTorque(textFieldTorque.getText());
				exportDTO.setPowerMotor(textFieldPowerMotor.getText());
				export.rollingExport(exportDTO);
			}
		});
		/*
		 * Przycisk służacy do powrotu do panelu inputPanel i zmiany wprowadzonych danych
		 */
		btnChange = new JButton("Zmiana danych");
		GridBagConstraints gbcBtnChange = new GridBagConstraints();
		gbcBtnChange.anchor = GridBagConstraints.WEST;
		gbcBtnChange.insets = new Insets(0, 0, 5, 5);
		gbcBtnChange.gridx = 2;
		gbcBtnChange.gridy = 5;
		resultsPanel.add(btnChange, gbcBtnChange);
		btnChange.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				inputPanel.setVisible(true);
				startPanel.setVisible(false);
				resultsPanel.setVisible(false);
				logger.info("Otwieranie panelu inputPanel w celu zmiany wprowadzonych danych.");
			}
		});
		/*
		 * Tworzenie nowego pliku
		 */
		btnNew = new JButton("Nowy");
		GridBagConstraints gbcBtnNew = new GridBagConstraints();
		gbcBtnNew.anchor = GridBagConstraints.EAST;
		gbcBtnNew.insets = new Insets(0, 0, 5, 5);
		gbcBtnNew.gridx = 1;
		gbcBtnNew.gridy = 6;
		resultsPanel.add(btnNew, gbcBtnNew);
		btnNew.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				logger.info("Otwieranie nowego pliku.");
				textFieldPlateIn.setText(" ");
				textFieldPlateOut.setText(" ");
				textFieldWidthIn.setText(" ");
				textFieldRe.setText(" ");
				textFieldDRoll.setText(" ");
				textFieldDNeck.setText(" ");
				textFieldNMotor.setText(" ");
				textFieldReduction.setText(" ");
				rdbtnNoLube.setSelected(true);
				rdbtnWithLube.setSelected(false);
				rdbtnCold.setSelected(true);
				rdbtnHot.setSelected(false);
				cbMat.setSelectedIndex(0);
				inputPanel.setVisible(true);
				startPanel.setVisible(false);
				resultsPanel.setVisible(false);
			}
		});
		/*
		 * Wyjście z programu z poziomu panelu resultsPanel
		 */
		btnQuit = new JButton("Zakończ");
		GridBagConstraints gbcBtnQuit = new GridBagConstraints();
		gbcBtnQuit.anchor = GridBagConstraints.WEST;
		gbcBtnQuit.insets = new Insets(0, 0, 5, 5);
		gbcBtnQuit.gridx = 2;
		gbcBtnQuit.gridy = 6;
		resultsPanel.add(btnQuit, gbcBtnQuit);
		btnQuit.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				logger.info("Wyłączenie programu");
				System.exit(0);
			}
		});
	}
}